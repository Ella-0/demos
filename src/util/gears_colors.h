#ifndef GEARS_COLORS_H
#define GEARS_COLORS_H

#ifdef USE_RAINBOW_GEARS_COLORS
#define GEARS_R1 0.8
#define GEARS_R2 0.1
#define GEARS_R3 0.0
#define GEARS_G1 0.0
#define GEARS_G2 0.8
#define GEARS_G3 0.2
#define GEARS_B1 0.2
#define GEARS_B2 0.2
#define GEARS_B3 1.0
#else
#define GEARS_R1 1.0
#define GEARS_R2 0.6
#define GEARS_R3 0.7
#define GEARS_G1 1.0
#define GEARS_G2 1.0
#define GEARS_G3 1.0
#define GEARS_B1 0.2
#define GEARS_B2 0.8
#define GEARS_B3 1.0
#endif

#endif /* GEARS_COLORS_H */
