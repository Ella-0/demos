/*
 * Copyright 2023 Ella Stanforth
 * SPDX-License-Identifier: MIT
 */

/*
 * Simple utility to query the gears colors
 */

#include <stdio.h>

#include "gears_colors.h"

int main(void) {
	printf("%f %f %f\n", GEARS_R1, GEARS_R2, GEARS_R3);
	printf("%f %f %f\n", GEARS_G1, GEARS_G2, GEARS_G3);
	printf("%f %f %f\n", GEARS_B1, GEARS_B2, GEARS_B3);
}
